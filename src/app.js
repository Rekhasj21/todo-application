import { getTodoListFromLocalStorage, saveTodoToLocalStorage } from './localStorage.js'

const todoItemsContainer = document.getElementById("todoItemsContainer");
const addButtonElement = document.getElementById("addButton");

const allSelected = document.getElementById("all");
const activeSelected = document.getElementById("active");
const completedSelected = document.getElementById("completed");

const saveButton = document.getElementById("saveButton")

let isCompletedClicked = false

let todoList = getTodoListFromLocalStorage();
let todoListCount = todoList.length;
console.log(todoListCount)
displayTodoList(todoList)

function toggleActive(button) {
    allSelected.classList.remove('active-list');
    activeSelected.classList.remove('active-list');
    completedSelected.classList.remove('active-list');
    button.classList.add('active-list');
}

allSelected.addEventListener('click', function () {
    document.getElementById('deleteAllIconContainer').style.visibility = 'hidden'
    document.getElementById("inputContainer").style.visibility = 'visible'
    isCompletedClicked = false
    toggleActive(allSelected);
    displayTodoList(todoList);

});

activeSelected.addEventListener('click', function () {
    document.getElementById('deleteAllIconContainer').style.visibility = 'hidden'
    document.getElementById("inputContainer").style.visibility = 'visible'
    const activeTodoList = todoList.filter(todo => todo.isActive);
    toggleActive(activeSelected);
    displayTodoList(activeTodoList);

});

completedSelected.addEventListener('click', function () {
    document.getElementById('deleteAllIconContainer').style.visibility = 'visible'
    document.getElementById("inputContainer").style.visibility = 'hidden'
    const completedTodoList = todoList.filter(todo => !todo.isActive);
    isCompletedClicked = true
    toggleActive(completedSelected);
    displayTodoList(completedTodoList);
});

addButtonElement.onclick = function () {
    let userEnteredTask = document.getElementById('userEnteredTask');
    let userInputTask = userEnteredTask.value.trim();

    todoListCount = todoListCount + 1;

    let todo = {
        text: userInputTask,
        uniqueNo: todoListCount,
        isActive: true,

    }

    todoList.push(todo);
    displayTodoList(todoList);
    userEnteredTask.value = '';
}

function createTodoItem(todo) {
    let id = todo.uniqueNo;
    let todoId = 'todo' + id;
    let labelId = 'label' + id;
    let checkboxId = 'checkbox' + id;

    let todoListItem = document.createElement('li');
    todoListItem.classList.add('todo-items');
    todoListItem.id = todoId;
    todoItemsContainer.appendChild(todoListItem);

    let inputElement = document.createElement('input');
    inputElement.type = 'checkbox';
    inputElement.id = checkboxId;
    inputElement.classList.add("checkbox");
    inputElement.checked = !todo.isActive;
    todoListItem.appendChild(inputElement);

    let labelElement = document.createElement('label');
    labelElement.setAttribute('for', checkboxId);
    labelElement.classList.add('label');
    labelElement.id = labelId;
    labelElement.textContent = todo.text;
    todoListItem.appendChild(labelElement);

    if (!todo.isActive) {
        labelElement.classList.add('checkbox-strike');
    }

    if (!todo.isActive && isCompletedClicked) {
        labelElement.classList.add('checkbox-strike');
        let deleteButtonElement = document.createElement("span");
        deleteButtonElement.textContent = "delete_outline";
        deleteButtonElement.classList.add("material-icons", "material");
        todoListItem.appendChild(deleteButtonElement);

        deleteButtonElement.onclick = function () {
            let index = todoList.findIndex((t) => t.count === todo.count);
            todoList.splice(index, 1);
            const completedTodoList = todoList.filter(todo => !todo.isActive);
            displayTodoList(completedTodoList);
        };
    }

    inputElement.onclick = function () {
        labelElement.classList.toggle('checkbox-strike');
        todo.isActive = !inputElement.checked;
        displayTodoList(todoList);
    }
}

function displayTodoList(todoList) {
    todoItemsContainer.textContent = '';
    for (let todo of todoList) {
        createTodoItem(todo);
    }
}

document.getElementById("deleteAllIconContainer").onclick = function () {
    todoList = todoList.filter((todoItem) => todoItem.isActive);
    displayTodoList();
}

saveButton.onclick = function () {
    saveTodoToLocalStorage(todoList)
}

