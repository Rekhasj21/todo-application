export function getTodoListFromLocalStorage() {
    let stringfiedTodoList = localStorage.getItem('todoList')
    let parseTodoList = JSON.parse(stringfiedTodoList)
    if (parseTodoList === null) {
        return []
    }
    else {
        return parseTodoList
    }
}

export function saveTodoToLocalStorage(todoList) {
    localStorage.setItem('todoList', JSON.stringify(todoList))
}